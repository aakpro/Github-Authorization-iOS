//
//  AccessTokenResponse.swift
//  iOS-Challenge
//
//  Created by Amir Abbas Kashani on 2/4/20.
//  Copyright © 2020 Farshad Mousalou. All rights reserved.
//

import UIKit

struct AccessTokenResponse {
    let accessToken: String?
    let tokenType: String?
    let scope: String?
}
