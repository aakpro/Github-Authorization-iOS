//
//  Permissions.swift
//  iOS-Challenge
//
//  Created by Amir Abbas Kashani on 2/7/20.
//  Copyright © 2020 Farshad Mousalou. All rights reserved.
//


import Foundation

struct Permissions
{
	let admin : Bool?
	let push : Bool?
	let pull : Bool?
}
