//
//  Verification.swift
//  iOS-Challenge
//
//  Created by Amir Abbas Kashani on 2/4/20.
//  Copyright © 2020 Farshad Mousalou. All rights reserved.
//

import Foundation

struct Verification {
	let verified : Bool?
	let reason : String?
	let signature : String?
	let payload : String?
}
