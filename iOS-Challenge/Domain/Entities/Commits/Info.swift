//
//  Info.swift
//  iOS-Challenge
//
//  Created by Amir Abbas Kashani on 2/4/20.
//  Copyright © 2020 Farshad Mousalou. All rights reserved.
//

import Foundation

struct Info {
	let author : Author?
	let committer : Committer?
	let message : String?
	let tree : Tree?
	let url : String?
	let comment_count : Int?
	let verification : Verification?
}
