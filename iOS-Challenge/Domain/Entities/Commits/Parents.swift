//
//  Parents.swift
//  iOS-Challenge
//
//  Created by Amir Abbas Kashani on 2/4/20.
//  Copyright © 2020 Farshad Mousalou. All rights reserved.
//

import Foundation

struct Parents {
	let sha : String?
	let url : String?
	let html_url : String?
}
